package com.example.memorygame

import android.app.AlertDialog
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.widget.Button
import kotlinx.android.synthetic.main.activity_game.*
import android.util.DisplayMetrics
import android.view.View
import android.widget.LinearLayout

class GameActivity : AppCompatActivity() {
    var btarr= arrayListOf<Button>()
    var finishtime=0
    var btOpen:Button?=null
    var btnImgIndex= intArrayOf(0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,11,11,12,12,13,13,14,14,15,15,16,16,17,17,18,18)
    var size=0
    var btarr_hide= arrayListOf<Button>()
    var timer :CountDownTimer?=null
    var position=0;
    var time=0
    var finishCount=0
    var cardAmount=0
    var imgbox = intArrayOf( R.drawable.pa, R.drawable.pb,
        R.drawable.pc, R.drawable.pd, R.drawable.pe, R.drawable.pf, R.drawable.pg, R.drawable.ph,R.drawable.p1,
        R.drawable.p2, R.drawable.p3, R.drawable.p4, R.drawable.p5, R.drawable.p6, R.drawable.p7, R.drawable.p8,
        R.drawable.p9, R.drawable.p10)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        dealIntent()
        cardAmount=size*size
        for(i in 0..cardAmount-1){
            val randPosition=Math.floor(Math.random()*cardAmount).toInt()
            position=btnImgIndex[i]
            btnImgIndex[i]=btnImgIndex[randPosition]
            btnImgIndex[randPosition]=position
        }
        makeCard()
        btn_cheat.setOnClickListener(){
            for(i in 0..cardAmount-1){
                if(btarr[i].hint!="OK"){
                    btarr[i].setBackgroundResource(imgbox[btarr[i].hint.toString().toInt()])
                    btarr_hide.add(btarr[i])
                }
            }
            val handler = Handler()
            handler.postDelayed({
                for(i in 0..btarr_hide.size-1)
                    btarr_hide[i].setBackgroundResource(R.drawable.back)
                btarr_hide.clear()
            }, 2000)
        }
    }
    fun dealIntent(){
        val intent=getIntent()
        size=intent.getIntExtra("size",2)
        time=intent.getIntExtra("time",30)
        timer=object :CountDownTimer((time*1000).toLong(),1000){
            override fun onTick(millisUntilFinished: Long) {
                tv_timer.setText("你還有"+(millisUntilFinished / 1000).toString() + "秒")
                finishtime = time - (millisUntilFinished / 1000).toInt()
            }
            override fun onFinish() {
                if(cardAmount!=finishCount)
                    finishGame("闖關失敗")
                timer!!.cancel()
            }

        }
        (timer as CountDownTimer).start()
    }
    fun makeCard(){
        //先取得DisplayMetrics資訊
        val mDisplayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(mDisplayMetrics)
        val mWidth = mDisplayMetrics.widthPixels
        val mHeight = mDisplayMetrics.heightPixels*3/4
        var width=mWidth/size
        var height=mHeight/size
        val Butparams = LinearLayout.LayoutParams(width, height)
        var b=Button(this)
        val btnListen=btnListener()//這mListen會產生物件，所以避免在迴圈中使用
        for(i in 0 .. cardAmount-1){
            var b=Button(this)
            btarr.add(b)
            if(size==2){
                when(i/2){
                    0->r1.addView(b)
                    1->r2.addView(b)
                }
            }
            else if(size==4){
                when(i/4){
                    0->r1.addView(b)
                    1->r2.addView(b)
                    2->r3.addView(b)
                    3->r4.addView(b)
                }
            }
            else{
                when(i/6){
                    0->r1.addView(b)
                    1->r2.addView(b)
                    2->r3.addView(b)
                    3->r4.addView(b)
                    4->r5.addView(b)
                    5->r6.addView(b)
                }
            }
            b.setOnClickListener(btnListen)
        }
        for(i in 0..cardAmount-1) {
            btarr[i].text = ""
            btarr[i].hint = btnImgIndex[i].toString()
            btarr[i].setBackgroundResource(R.drawable.back)
            btarr[i].setLayoutParams(Butparams)
            btarr[i].setHintTextColor(Color.parseColor("#00000000"))
        }
    }
    private inner class btnListener: View.OnClickListener{
        override fun onClick(v:View){
            val b=v as Button
            b.setBackgroundResource(imgbox[b.hint.toString().toInt()])
            if(btOpen==null)//翻第一張牌
                btOpen=b
            else{//翻第二章牌
                if(btOpen!!.hint==b.hint){//兩張牌一樣
                    btOpen!!.hint="OK"
                    b.hint="OK"
                    btOpen=null
                    finishCount+=2

                    if(finishCount==cardAmount){
                        finishGame("闖關成功")
                        timer!!.cancel()
                    }
                }
                else{//兩張牌不一樣
                    val handler = Handler()
                    handler.postDelayed({
                        btOpen!!.setBackgroundResource(R.drawable.back)
                        b.setBackgroundResource(R.drawable.back)
                        btOpen=null
                    }, 1000)
                }
            }
        }
    }

    internal fun finishGame(title: String) {
        val builder = AlertDialog.Builder(this)

        builder.setMessage("您花了" + finishtime + "秒鐘，完成了" + finishCount/2 + "對牌")
            .setTitle(title)
        builder.setPositiveButton("結束") { dialog, id ->
            this.finish()
            // User clicked OK button
        }
        val dialog = builder.create()
        dialog.show()

    }

}
